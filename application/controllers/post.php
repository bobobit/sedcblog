<?php

class post extends CI_Controller{

	public function index(){
		$output=array();
		$output['header']="All posts";
		$output['rezultati']=$this->postmodel->all_posts();
		$this->load->view("all_posts.php",$output);
	}
	
	public function add(){
		$this->load->view('post_add_form.php');
	}
	
	public function save(){
		$_POST['created']=date("Y-m-d H:i:s");
		if($this->db->insert('post',$_POST)){
			redirect("http://localhost/blog/index.php/post/index");
		}
	}
	
	public function view(){
		$output=array();
		$posts=$this->postmodel->get_post_from_url();
		$output['post']=$posts[0];
		$output['comments']=$this->postmodel->get_comments();
		$this->load->view('view_post.php',$output);
	}

	public function insert_comment(){
		$_POST['created']=date("Y-m-d H:i:s");
		if($this->db->insert('comments',$_POST)){
			redirect("http://localhost/blog/index.php/post/view/".@$_POST['post_id']);
		}
	
	}

}