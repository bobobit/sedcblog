<?php

class postmodel extends CI_Model{

	function latest_posts($num=5){
		return $this->db->query("SELECT * FROM post ORDER BY created DESC LIMIT ".$num)->result_array();
	}
	function all_posts(){
		return $this->db->query("SELECT * FROM post ORDER BY created DESC")->result_array();
	}

	function get_post_from_url(){
		return $this->db->query("SELECT * FROM post WHERE id=".$this->uri->segment(3)." LIMIT 1")->result_array();
	}

	function get_comments(){
		return $this->db->query("SELECT * FROM comments WHERE post_id=".$this->uri->segment(3)." ORDER BY created ASC")->result_array();
	}

}